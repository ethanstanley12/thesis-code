#!/usr/bin/env python3

import subprocess
import argparse

# toggle these to control which systems are under test
targets = [
           "wasmtime",
           "wasmer",
        #    "wasmedge",
           "wasmi",
           "wamr",
           "x86"
          ]


# no need to change these
run_cmds = {"x86" : "./harness",
            "wasmtime" : "wasmtime --allow-precompiled --dir=. harness.wasm",
            "wasmer" : "wasmer --dir=. harness.wasm",
            "wasmedge" : "wasmedge --dir=. harness.wasm",
            "wasmi" : "wasmi_cli --dir=. harness.wasm",
            "wamr" : "iwasm --dir=. harness.wasm"}

parser = argparse.ArgumentParser(
                    prog='go-button.py',
                    description='Runs a fuzzing campaign using wasimilar program generator',
                    epilog='Author: Ethan Stanley')

parser.add_argument('-s', '--seed')
# parser.add_argument('--no-gen', action='store_true', help="Do not generate new test cases. Use what is currently in harness/src/main.rs.")
parser.add_argument('-n', '--num-tests', default='1')
parser.add_argument('mode', choices=['fuzz', 'test', 'reduce'])
parser.add_argument('-f', '--file')

args = parser.parse_args()

gen_command = ["racket", "../wasimilar/wasimilar.rkt"]

if args.file is not None:
    if not (args.mode == 'reduce' or args.mode == 'test'):
        print("Cannot specify --file when generating tests")
        exit(-1)
    else:
        subprocess.run(["cp", args.file, "src/main.rs"],
                       stdout=subprocess.DEVNULL)



if args.seed is not None:
    gen_command.append("--seed")
    gen_command.append(args.seed)

clear_command = ["./scripts/clear-sandboxes.sh"] + targets

if args.mode == 'fuzz':
    print("Compiling wasimilar...")
    subprocess.run(["raco", "make", "../wasimilar/wasimilar.rkt"])

test_cases_tried = 0
bugs_found = 0
try:
    if args.mode == 'fuzz':
        print('Starting campaign...')
    with open("logs/go-button.log", 'a') as log, open("logs/bugs-found.log", "a") as bug_log:
        while test_cases_tried < int(args.num_tests) or int(args.num_tests) == 0:
            if args.mode == 'fuzz':
                subprocess.run(gen_command,
                               stdout=open("src/main.rs", "w"))
            
            build_code = subprocess.run(["./scripts/build.sh"],
                                        stdout=log,
                                        stderr=log).returncode
            if args.mode == 'reduce' and build_code != 0:
                # breaking change was made to test case
                # exit w/ non-zero return code to signal reducer
                exit(2)
            
            subprocess.run(clear_command)

            for target in targets:
                subprocess.run(["scripts/run-target.sh", target, run_cmds[target]],
                                stdout=log,
                                stderr=log)

            result = 0
            for target in targets:
                if target == "x86":
                    continue
                result += subprocess.run(["./scripts/predicate.sh", target], 
                                         stdout=log, 
                                         stderr=log).returncode
            if result != 0:
                if args.mode == "fuzz":
                    header = subprocess.run(["head", "-5", "src/main.rs"], capture_output=True, text=True).stdout
                    seed = header.split("\n")[-2].strip().split()[-1]
                    bug_log.write(seed + "\n")
                bugs_found += 1
            
            test_cases_tried += 1
except KeyboardInterrupt:
    pass
if args.mode == 'reduce':
    if bugs_found > 0:
        exit(0)
    else:
        exit(1)
elif args.mode == 'fuzz':
    print('Done.\nFound {} bug{} using {} test case{}'.format(bugs_found,
                                                            "" if bugs_found == 1 else "s",
                                                            test_cases_tried,
                                                            "" if test_cases_tried == 1 else "s"))
elif args.mode == 'test':
    print('Tried main.rs {} time{}, outputs differed {} time{}'.format(test_cases_tried,
                                                                       "" if test_cases_tried == 1 else "s",
                                                                       bugs_found,
                                                                       "" if bugs_found == 1 else "s"))