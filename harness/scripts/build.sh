#!/bin/bash

rustfmt src/main.rs
echo -e "\n---- building x86 ----"
cargo build --bin harness --release
x86=$?
echo "---- building wasm32-wasi ----"
cargo build --bin harness --target wasm32-wasi --release
wasi=$?
# return 0 iff both targets built successfully
if [ $x86 -eq 0 ] && [ $wasi -eq 0 ];
then
    exit 0
else
    exit 1
fi