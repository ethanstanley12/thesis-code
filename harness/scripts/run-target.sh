#!/bin/bash

echo "---- running " $1 "----"

if [ "$1" == "x86" ]
then
    cp target/release/harness sandboxes/x86-sandbox
else
    cp target/wasm32-wasi/release/harness.wasm sandboxes/$1-sandbox
fi

cd ./sandboxes/$1-sandbox
$2 > stdout.log

if [ "$1" == "wasmi" ]
then
    cat stdout.log | tail -n +2 > temp_stdout.log
    cp temp_stdout.log stdout.log
    rm temp_stdout.log
fi