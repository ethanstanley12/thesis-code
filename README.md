Compile rust programs to WASI with:
	$ cd TEST_CASE_DIR
	$ cargo build --target wasm32-wasi
	$ wasmtime target/wasm32-wasi/debug/TEST_CASE_DIR.wasm

To compile/run to assembly, use:
	$ cd TEST_CASE_DIR
	$ cargo run
