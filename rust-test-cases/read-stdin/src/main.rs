use std::io;

fn main() {
	let mut buffer = String::new();
	io::stdin().read_line(&mut buffer).expect("error reading stdin");
	println!("you said: {}", buffer);
}
