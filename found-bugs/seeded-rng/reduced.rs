// This is a RANDOMLY GENERATED PROGRAM.
// Fuzzer: wasimilar
// Version: wasimilar, xsmith 2.0.6 (de6ace1), in Racket 8.2 (vm-type chez-scheme)
// Options: --seed 1
// Seed: 1
//


use rand::{Rng,SeedableRng};
use rand::rngs::StdRng;

fn main() -> std::io::Result<()>{
    let mut rng = StdRng::seed_from_u64(568596);
    let open_file_ptrs = vec![0];
    rng.gen_range(0..open_file_ptrs.len());
    println!("{}", rng.gen_range(0..2));
    Ok(())
}


